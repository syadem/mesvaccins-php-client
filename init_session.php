<?php
  require 'vendor/autoload.php';
  require_once 'Zend/Http/Client.php';
  require_once 'Zend/Debug.php';

  define('INTEGRATION_URL', 'https://test-pro-secure.mesvaccins.net/authenticationids/restloginservice.php');
  define('PRODUCTION_URL', 'https://pro-secure.mesvaccins.net/authenticationids/restloginservice.php');

  $applicationUid = $argv[1];
  $email = $argv[2];
  $organisationId = $argv[3];
  $environment = isset($argv[4]) ? $argv[4] : '';
  $environmentUrl = $environment == 'production' ? PRODUCTION_URL : INTEGRATION_URL;
  $certificateName = "certificate".$environment.".pem";

  function get_client_with_certificate($certificateName) {
    return new Zend_Http_Client(
      null,
      array(
        'adapter' => 'Zend_Http_Client_Adapter_Socket',
        'sslcert' => dirname(__FILE__) . DIRECTORY_SEPARATOR . $certificateName,
        'ssltransport' => 'tls'
      )
    );
  }

  function generate_password($email, $organisationId, $applicationUid) {
    return crypt($email.$organisationId, "$2a$10$".$applicationUid);
  }

  $client = get_client_with_certificate($certificateName);
  $password = generate_password($email, $organisationId, $applicationUid);
  $request = $client
    ->setUri($environmentUrl)
    ->setRawData('{"authentifier": "'.$email.'", "password": "'.$password.'"}')
    ->setHeaders("Content-Type", "application/json");
  $response = $request->request(Zend_Http_Client::POST);
  $response_body = json_decode($response->getBody());
  if ($response_body->status === 0) {
    echo "Session id : " . $response_body->sessionids;
  } else if (isset($response_body->status)) {
    echo "Erreur : status=" . $response_body->status;
  } else {
    echo "Erreur : " . $response->getBody();
  }
?>
