# Client MesVaccins en PHP

Cette implémentation est fournie à titre d'exemple uniquement.

## Dépendances

Ce projet utilise [Zend Framework](http://framework.zend.com) pour la connexion au webservice.

Pour installer les dépendances à l'aide de [composer](https://getcomposer.org), exécutez `php composer.phar install`.

## Manual d'intégration

Un manuel d'intégration décrit les fonctionnalités de notre API et les modalités de leur utilisation : [https://www.mesvaccins.net/manual/](https://www.mesvaccins.net/manual/).

## Nous contacter

Vous pouvez joindre le support développeurs de MesVaccins.net à [developers@mesvaccins.net](mailto:developers@mesvaccins.net).