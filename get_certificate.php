<?php
require 'vendor/autoload.php';
require_once 'Zend/Http/Client.php';

define('INTEGRATION_URL', 'https://certtest.idshost.fr/restgetcertificate.php');
define('PRODUCTION_URL', 'https://cert.idshost.fr/restgetcertificate.php');

$otp = $argv[1];
$environment = isset($argv[2]) ? $argv[2] : '';
$environmentUrl = $environment == 'production' ? PRODUCTION_URL : INTEGRATION_URL;
$certificateName = "certificate".$environment.".pem";

$request = new Zend_Http_Client($environmentUrl);
$request->setRawData('{"identifier": "none", "otp": "'.$otp.'", "type": "pem"}');
$request->setHeaders("Content-Type", "application/json");
$response = $request->request(Zend_Http_Client::POST);
$response_body = json_decode($response->getBody());
if ($response_body->status === 0) {
  $decodedCertificate = base64_decode($response_body->certbase64encoded);
  $certificate_file = fopen($certificateName, "wb") or die("Unable to open file!");
  fwrite($certificate_file, $decodedCertificate);
  fclose($certificate_file);
} else if (isset($response_body->status)) {
  echo "Erreur : status=" . $response_body->status;
} else {
  echo "Erreur : " . $response->getBody();
}
?>
